package fr.manj.niveau4.ex2;

public class Ex2 {

	public static void main(String[] args) {
		// Ajouter un constructeur a personne pour prendre le nom en paramètre.
		Personne p = new Personne();

		// Ecrire la méthode toString de personne pour qu'elle renvoie le nom.
		System.out.println(p);
	}

}
