package fr.manj.niveau4.ex01;

public class Point {
	private double x;
	private double y;

	public Point() {
		this.x = 0;
		this.y = 0;
	}

	// Corrigez le constructeur Point(x,y) pour qu'il fonctionne :
	// Expliquez pourquoi il y'a un pb.
	public Point(int x, int y) {
		this.x = x;
		y = y;
	}

	public void afficherPoint() {
		System.out.println("[" + this.x + "," + this.y + "]");
	}

}
