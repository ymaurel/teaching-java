package fr.manj.niveau4.ex01;

public class Ex1 {

	public static void question1() {
		// Creez un simple point de façon à ce que le code fonctionne :
		Point p1 = null;
		p1.afficherPoint();
	}

	public static Point question2() {
		// 2. Ecrire un constructeur Point(x,y)
		// qui permet d'initialiser l'état de l'objet
		// Donnez la valeur x = 1 et y = 4
		Point p2 = null;
		p2.afficherPoint();
		return p2;
	}

	public static void main(String[] args) {
		question1();
		question2();
	}
}
