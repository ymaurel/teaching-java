package fr.manj.niveau3.ex3;

public class StructureConditionnelle {

	enum Animal {
		CHAT, CHIEN, AUTRE
	}

	/**
	 * Implémentez
	 * 
	 * @return l'enum correspondant à l'animal
	 * 
	 */
	public static Animal stringToAnimal(String nom) {
		// Pensez a mettre la chaine en minuscule avant de comparer

		// Implémenter avec un If then else

		// On compare les chaines avec equals !

		//if (nom.toLow....
				
				
		return Animal.AUTRE;
	}

	/**
	 * Méthode qui fait l'inverse (en utilisant un switch).
	 * 
	 * @param a
	 * @return
	 */
	public static String animalToString(Animal a) {
//		switch (a) {
//		case CHIEN:
//			;
//		default:
//			return "je sais pas";
//		}
		return null;
	}

	public static void main(String[] args) {
		System.out.println(stringToAnimal("Chien"));
		System.out.println(stringToAnimal("cHaT"));
		System.out.println(stringToAnimal("Oiseau"));

		System.out.println(animalToString(Animal.CHIEN));
		System.out.println(animalToString(Animal.CHAT));
		System.out.println(animalToString(Animal.AUTRE));
		
	}
}
