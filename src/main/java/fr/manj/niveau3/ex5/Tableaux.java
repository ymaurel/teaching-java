package fr.manj.niveau3.ex5;

import java.util.Arrays;

//Ecrire les méthodes suivantes en utilisant des boucles
public class Tableaux {
	
	/**
	 * Retourne le nombre de notes :
	 */
	static long count(int[] notes) {
		return 0;
	}
	
	/**
	 * Retourne la somme des notes avec une boucle pour tout (foreach)
	 */
	static long sum(int[] notes) {
		return 0;
	}
	
	/**
	 * Retourne la moyenne des notes avec une boucle for classique
	 */
	static double avg(int[] notes) {
		return 0;
	}
	
	/**
	 * Retourne le max
	 */
	static int max(int[] notes) {
		return 0;
	}
	
	/**
	 * Retourne le min
	 */
	static int min(int[] notes) {
		return 0;
	}
	

	
	/**
	 * Retourne une copie triée du tableau
	 * Attention ! ne dois pas modifier le tableaux original !
	 */
	static int[] sort(int[] notes) {
		//On déclare une copie
		
		//On utilise la primitive system pour copier le contenu
        
        //On trie la copie avec Arrays.sort
		
		//On retourne la copie 
		return new int[] {0,0,0};
	}
	
	/**
	 * Retourne vraie si le tableau est trié
	 */
	static boolean isSorted(int[] notes) {
		return false;
	}
	
	public static void main(String[] args) {
		//Ecrire la déclaration d'un tableaux de plusieurs notes :
		int[] notes = null; //new int[]....
		System.out.println("Le nombre de notes est : "+ count(notes));
		System.out.println("La somme est : "+ sum(notes));
		System.out.println("La moyenne est : "+ avg(notes));
		System.out.println("Le max est : "+ max(notes));
		System.out.println("Le min est : "+ min(notes));
	}
	
}
