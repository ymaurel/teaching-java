package fr.manj.niveau3.ex6;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

//Ecrire les méthodes suivantes en utilisant des boucles
public class AvecDesListes {
	
	/**
	 * Retourne le nombre de notes :
	 */
	static long count(List<Integer> notes) {
		return 0;
	}
	
	/**
	 * Retourne la somme des notes avec une boucle pour tout (foreach)
	 */
	static long sum(List<Integer> notes) {
		return 0;
	}
	
	/**
	 * Retourne la moyenne des notes avec une boucle for classique
	 */
	static double avg(List<Integer> notes) {
		return 0;
	}
	
	/**
	 * Retourne le max
	 */
	static int max(List<Integer> notes) {
		return 0;
	}
	
	/**
	 * Retourne le min
	 */
	static int min(List<Integer> notes) {
		return 0;
	}
	

	
	/**
	 * Retourne une copie triée du tableau
	 * Attention ! ne dois pas modifier le tableaux original !
	 */
	static List<Integer> sort(List<Integer> notes) {
		//On déclare une copie et on copie la liste
		        
        //On trie la copie avec Arrays.sort
		
		//On retourne la copie 
		return Arrays.asList(3,1,2);
	}
	
	/**
	 * Retourne vraie si le tableau est trié
	 */
	static boolean isSorted(List<Integer> notes) {
		return false;
	}
	
	public static void main(String[] args) {
		//Ecrire la déclaration d'une liste de plusieurs notes :
		List<Integer> notes = null; //new ArrayList()...
		
		//Remplir la liste :
		//notes.add(12); 

		System.out.println("Le nombre de notes est : "+ count(notes));
		System.out.println("La somme est : "+ sum(notes));
		System.out.println("La moyenne est : "+ avg(notes));
		System.out.println("Le max est : "+ max(notes));
		System.out.println("Le min est : "+ min(notes));
	
		//Essayez le programme avec LinkedList et voir si cela fonctionne toujours !
	
	}
	
}
