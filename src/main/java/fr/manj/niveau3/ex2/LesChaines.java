package fr.manj.niveau3.ex2;

public class LesChaines {

	/**
	 *  Tester l'égalité entre les deux chaines s1 = s2 ?
	 *  Vrai si le contenu est identique.
	 * @return
	 */
	public static boolean egales(String s1, String s2) {
		return false;
	}

	//Retourne vrai si la variable pointe sur la même case mémoire.
	public static boolean identique(String s1, String s2) {
		return false;
	}
	
	/**
	 * Implémentez la méthode :
	 * @param s1 : chaine 1 a mettre en minuscule
	 * @param s2 : chaine 2 a mettre en majuscule
	 * @return	s1 en minuscule, un espace suivi de s2 en majuscule
		example s1="John" s2 ="Doe" retourne "john DOE"
	 */
	public static String transforme(String s1, String s2) {
		return "";
	}
	
	public static void main(String[] args) {
		String s1 = "Test";
		String s2 = "Test";
		String s3 = new String("Test");
		String s4 = "Toto";
		String s5 = s1;
		
		//Vérifiez que vous obtenez bien le résultat entre parenthèse.
		System.out.printf("%s = %s ? %b (true)\n",s1,s2, egales(s1, s2));
		System.out.printf("%s = %s ? %b (true)\n",s1,s3, egales(s1, s3));
		System.out.printf("%s = %s ? %b (false)\n",s1,s4, egales(s1, s4));
		System.out.printf("%s = %s ? %b (true)\n",s1,s5, egales(s1, s5));

		System.out.printf("%s identique a %s ? %b (true ou false selon la machine)\n",s1,s2, identique(s1, s2));
		System.out.printf("%s identique a %s ? %b (false)\n",s1,s3, identique(s1, s3));
		System.out.printf("%s identique a %s ? %b (false)\n",s1,s4, identique(s1, s4));
		System.out.printf("%s identique a %s ? %b (true)\n",s1,s5, identique(s1, s5));
		
		System.out.println(transforme("John", "DoE")+ " == john DOE ?");

	}
}
