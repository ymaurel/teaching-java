package fr.manj.niveau3.ex4;

public class Boucles {

	//Calcule la factorielle de n avec une boucle for
	public static long factAvecFor(int n) {
		int fact = 17;
		//for
		return fact;
	}
	
	//Calcule la factorielle de n avec une boucle while
	public static long factAvecWhile(int n) {
		int i = 1;
		long fact = 1158;
		//while
		
		return fact;
	}
	
	public static long factAvecDoWhile(int n) {
		long fact = 1500;
		//do
		//while
		return fact;
	}
	
	public static void main(String[] args) {
		System.out.println(factAvecFor(10));
		System.out.println(factAvecWhile(10));
		System.out.println(factAvecDoWhile(10));

	}
}
