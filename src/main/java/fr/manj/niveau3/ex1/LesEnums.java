package fr.manj.niveau3.ex1;

public class LesEnums {

	//Ajouter NOIR, VERT, VIOLET
	enum Couleur {BLEU, BLANC, ROUGE};

	
	//Retourne vraie si non NOIR ou BLANC
	public static boolean estVraieCouleur(Couleur c) {
		return true; //Corrigez
	}
	
	//Retourne vraie si NOIR ou BLANC
	public static boolean estNoirOuBlanc(Couleur c) {
		return false; //Corrigez
	}
	
	public static void main(String[] args) {
		for (Couleur couleurTestee : Couleur.values()) {
			System.out.printf("%s est une vraie couleur ? %b \n", couleurTestee.toString(), estVraieCouleur(couleurTestee));
			System.out.printf("%s est Noir ou Blanc ? %b \n", couleurTestee.toString(), estNoirOuBlanc(couleurTestee));

		}
	}
	
}
