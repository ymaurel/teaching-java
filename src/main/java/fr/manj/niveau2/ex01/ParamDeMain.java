package fr.manj.niveau2.ex01;

public class ParamDeMain {

	/**
	 * Modifier nom et prénom pour que prenom = premier parametre et nom deuxieme
	 *
	 * Pour passez des arguments à Eclipse, allez dans "Run Configurations (fleche
	 * verte ci-dessus) puis "Program arg" dans l'onglet Arguments. (cf
	 * https://www.cs.colostate.edu/helpdocs/eclipseCommLineArgs.html)
	 *
	 * @param args
	 */

	public static void main(String[] args) {
		String prenom = "John";
		String nom = "Doe";
		System.out.println("Hello " + prenom + " " + nom);
	}

}