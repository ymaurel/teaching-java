package fr.manj.niveau2.ex02;

public class CalculDePi {

	
	/**
	 * Cette fonction retourne 0 (bizarrement ?)
	 * 
	 * Corrigez la fonction ci-dessous pour quelle calcul Pi comme indiquez ici :
	 * https://fr.wikipedia.org/wiki/Formule_de_Leibniz#S%C3%A9rie_altern%C3%A9e
	 * @param n
	 * @return
	 */
	public static double calculPi(int n) {
		double pi = 0;
		
		//Les bornes sont-elles correctes ?
		for(int i = 1; i < n; i++) {
			// La formule est-elle correcte (il manque une operation non ?)
			pi = (Math.pow(-1, i)/(2*i+1));
		}
		return pi*4;
	}
	
	public static void main(String[] args) {
		//N dois etre très grand pour que la formule donne un résultat approchant
		double piEstimation = calculPi(1000);
		System.out.println(piEstimation + " devrait etre environ 3.14");
	}
	
}
