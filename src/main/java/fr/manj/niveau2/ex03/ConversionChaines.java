package fr.manj.niveau2.ex03;

public class ConversionChaines {

	public static void main(String[] args) {
		
		//Pour convertir une chaine en double on peut utiliser ValueOf :
		String temperatureChaine ="9.2";
		double temperature = Double.valueOf(temperatureChaine);
		
		//On peut ainsi afficher la temperature :
		System.out.println("La temperature est de "+ temperature +"°C");

		// On peut également utiliser la fonction printf pour afficher
		// Ici %f nous sert de marqueur pour inserer temperature passé en parametre
		// \n est un saut de ligne
		System.out.printf("La temperature est de %f °C \n", temperature);
		
		// Notez également que l'on peut passer limiter le nombre de décimale (%.2f pour deux chiffres après la virgule) :
		System.out.printf("La temperature est de %.2f °C \n", temperature);

		// Il existe aussi String.format qui permet d'obtenir la chaine sans l'afficher :
		String maChaine = String.format("Affiche %d et %.1f", 1, 1.234);
		System.out.println(maChaine);
		
		
		// Les fonctions suivantes sont a corriger :
		afficheAvecTroisChiffresApresLaVirgure(9.12520001);
		System.out.println(retourneLaChaineAvecUnChiffresApresLaVirgure(9.12520001));

	}
	
	
	// Completez les méthodes suivantes :
	public static float stringToFloat(String a) {
		return Float.valueOf(a);
	}
	
	/**
	 * Affiche 9.212 si c = 9.2125315
	 */
	
	public static void afficheAvecTroisChiffresApresLaVirgure(double c) {
		// sans saut de ligne 
		System.out.printf("...",c);
	}
	
	public static String retourneLaChaineAvecUnChiffresApresLaVirgure(double c) {
		return  ""; // /*String.f... */
	}
}
