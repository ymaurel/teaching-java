package fr.manj.niveau2.ex05;

public class ConversionsFloats {

	/**
	 * Ecrire la fonction division entiere 
	 */
	public static int divisionEntiere(int a, int b) {
		return 0;
	}
	
	/**
	 * Ecrire la fonction division flottant suivante 
	 */
	public static float division(int a, int b) {
		return 0;
	}
	
	
	/**
	 * Ecrire la fonction de conversion de °C vers °F
	 * (https://fr.wikipedia.org/wiki/Degr%C3%A9_Fahrenheit)
	 * @param temperature en celcius
	 * @return temperature en Fahrenheit = 9/5*celcius+32
	 */
	public static double celciusToFahrenheit(int celcius) {
		return 0;
	}
	
	
	public static void main(String[] args) {
		System.out.printf("float compris dans\t[%f,%f] \n", Float.MIN_VALUE, Float.MAX_VALUE);
		System.out.printf("double compris dans\t[%f,%f] \n", Double.MIN_VALUE, Double.MAX_VALUE);
	
		//Attention aux bornes :
		float max = Float.MAX_VALUE;
		System.out.printf("%f * %f = %f \n", max ,max, max * max);
		
		//Vérifiez que vos résultats sont cohérents :
		for(int i = 0; i <10; i++) {
			System.out.printf("[entiere  ] %d/%d = %d \n", i, 2, divisionEntiere(i,2));
			System.out.printf("[flottante] %d/%d = %.2f \n", i, 2, division(i,2));
		}	
		
		//Vérifier que 10°C = 50°F (valeur correcte)
		System.out.printf("10°c = %f °F", celciusToFahrenheit(10));

		
	}

	

}
