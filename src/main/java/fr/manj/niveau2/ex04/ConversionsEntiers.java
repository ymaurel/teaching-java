package fr.manj.niveau2.ex04;

public class ConversionsEntiers {

	public static void main(String[] args) {
		
		// On peut toujours faire tenir un nombre plus petit dans un plus grand :
		
		//sur 8 bits :
		byte unByte = -7;
		//sur 16 bits
		short unShort = unByte;
		//sur 32 bits
		int unEntier = unShort;
		//sur 64 bits :
		long unLong = unEntier;
		
		System.out.printf("%d == %d (%b) \n", unLong, unShort, unLong == unShort);
		
		///********
		
		// L'inverse n'est pas correct :
		long unAutreLong = 125;
		// short a = unAutreLong; // si vous décommentez la ligne vous aurez une erreur.
		
		///********
		
		//Mais on peut forcer la conversion avec un cast :
		short unAutreShort = (short) unAutreLong;
		
		//Ca fonctionne bien si la taille est compatible :
		System.out.printf("%d == %d (%b) \n", unAutreLong, unAutreShort, unAutreLong == unAutreShort);

		//Si la taille n'est pas compatible, le nombre est tronqué :
		unAutreLong = 70000;
		unAutreShort = (short) unAutreLong;
		System.out.printf("%d == %d (%b) \n", unAutreLong, unAutreShort, unAutreLong == unAutreShort);

	
		//On peut avoir les bornes des types :
		System.out.printf("byte compris dans\t[%d,%d] \n", Byte.MIN_VALUE, Byte.MAX_VALUE);
		System.out.printf("short compris dans\t[%d,%d] \n", Short.MIN_VALUE, Short.MAX_VALUE);
		System.out.printf("int compris dans\t[%d,%d] \n", Integer.MIN_VALUE, Integer.MAX_VALUE);
		System.out.printf("long compris dans\t[%d,%d] \n", Long.MIN_VALUE, Long.MAX_VALUE);

		
		//On vous demande décrire la fonction suivante :
		afficheLaConversionOuErreurSiTropGrand(15);
		afficheLaConversionOuErreurSiTropGrand(156000);
		
	}

	
	/**
	 * La fonction affiche ERREUR si le nombre est plus grand qu'un byte
	 * sinon elle affiche le nombre.
	 * @param i
	 */
	public static void afficheLaConversionOuErreurSiTropGrand(int i) {
		if(true) { // verifier que i rentre dans short
			byte c = 0; // faire la conversion de i
			System.out.println(c);
		}else {
			System.out.println("ERREUR");
		}
	}
	
	/**
	 * Ecrire la fonction multiplication suivante 
	 * Pourquoi cela pose-t-il probleme ?
	 * @param a 
	 * @param b
	 * @return a*b
	 */
	public static byte multiply(byte a, byte b) {
		return 0;
	}
	
}
