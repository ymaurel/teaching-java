package fr.manj.niveau1.ex08;

public class LocalOuDeClasse {
	
	public static int compteur1 = 0;
	
	public static void ajouteAuCompteur1() {
		compteur1 = compteur1+1;
	}

	public static void ajouteAuCompteur2(int compteur2) {
		compteur2 = compteur2 + 1;
	}
		
	
	/***
	 * A) Observez bien le résultat de ce code :
	 * Essayez de comprendre pourquoi compteur2 n'augmente jamais et compteur1 augmente.
	 */
	public static void main(String[] args) {
		
		 int compteur2 = 0; 
		 
		 //on itere 10 fois :
		 for(int i = 0; i <10; i++) {
			 ajouteAuCompteur1();
			 ajouteAuCompteur2(compteur2);
		 }
		 
		 System.out.println(compteur1);
		 System.out.println(compteur2);
	}
	
	
	/**
	 * B) Ecrire une méthode qui retourne compteur1+10 sans le modifier
	 * @return compteur1 plus 10
	 */
	public static int retourneCompteur1Plus10() {
		return compteur1 + 10;
	}
	
	/**
	 * C) Ecrire une méthode qui modifier compteur1 sans le retourner.
	 */
	public static void ajoute10ACompteur1() {
		compteur1 = compteur1+10;
	}
	
}
