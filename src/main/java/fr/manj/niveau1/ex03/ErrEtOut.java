package fr.manj.niveau1.ex03;

public class ErrEtOut {

	/**
	 * Ecrire une méthode main qui affiche "Hello" sur out et "World" sur err sans saut de ligne ni espace
	 */
	public static void main(String[] args) {
		System.err.print("Vous devrez utiliser les deux sorties");
	}
}
