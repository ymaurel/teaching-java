package fr.manj.niveau1.ex04;

public class LireLesParametres {

	public static void main(String[] args) {
		//Modifier cette ligne pour que la méthode affiche 4
		int i = 1;
		
		//Ne pas toucher a ces lignes :
		i = i+1;
		System.out.print(i);
	}
	
}
