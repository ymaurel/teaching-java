package fr.manj.niveau1.ex06;


/***
 * Une méthode statique s'appelle en préfixant par le nom de la classe :
 * 
 * Exemple : Math.sin(90) calcule le sin de 90.
 * 
 * Si on appelle la méthode depuis la même classe, il n'est pas nécessaire de préfixer.
 *
 */

public class AppelMethodeStatique {
	
	
	//Corrigez la fonction pour quelle retourne le bon résultat
	public static int add(int a, int b) {
		return a-b;
	}
	
	public static void main(String[] args) {
		int a = 3;
		int b = 5;
		//Appelez la fonction add pour additionner a et b et affecter le résultat à résultat.
		int resultat = 0;
		System.out.println(resultat);
		
	}
}
