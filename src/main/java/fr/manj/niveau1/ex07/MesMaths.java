package fr.manj.niveau1.ex07;

//Cette classe vous est fournie
public class MesMaths {
	
	/**
	 * La racine carrée inverse rapide (en anglais fast inverse square root, parfois abrégé Fast InvSqrt() 
	 * ou par la constante 0x5f3759df en hexadécimal) est une méthode pour calculer x−½, 
	 * l'inverse de la racine carrée d'un nombre à virgule flottante à simple précision sur 32 bits. 
	 * L'algorithme a probablement été développé chez Silicon Graphics au début des années 1990. 
	 * Il a entre autres été utilisé dans le code source de Quake III Arena, un jeu vidéo sorti en 19991. 
	 * @param x : le nombre x
	 * @return la racine carrée inverse
	 */
	public static double invSqrt(double x) {
	    double xhalf = 0.5d * x;
	    long i = Double.doubleToLongBits(x);
	    i = 0x5fe6ec85e7de30daL - (i >> 1);
	    x = Double.longBitsToDouble(i);
	    x *= (1.5d - xhalf * x * x);
	    return x;
	}
}
