package fr.manj.niveau1.ex07;

public class AppelMethodeMath {
	
	
	public static double invSqrtPlus1(int a) {
		double invSqrt = 0; //appel a la méthode invSqrt de MesMaths
		return invSqrt + 1;
	}
	
	/**
	 * Cette fonction doit retourner cos(a)+b
	 * Aidez vous de la javadoc de Math pour le calcul de cos
	 * @return cos(a)+b
	 */
	public static double cosAPlusB(int a, int b) {
		return 0; //appelez la méthode cos de Math pour le calcul de a
	}
	
	//Cette méthode n'est pas à vérifier :
	public static void main(String[] args) {
		//Les égalités ci dessous doivent se vérifiée si vous modifier correctement les méthodes ci-dessus:
		System.out.println(cosAPlusB(0,1) +" = 2 ?");
		System.out.println(invSqrtPlus1(4) +" est environ égal à  "+ (Math.pow(4, -0.5)+1) + " ?");
	}
}
