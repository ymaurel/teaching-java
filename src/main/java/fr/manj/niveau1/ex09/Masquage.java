package fr.manj.niveau1.ex09;

public class Masquage {

	public static int c = 0;
	
	public static void enleve(int b) {
		c -= b; //cette écriture est équivalente a c = c - b;
	}
	
	
	public static void ajoute(int c) {
		c += c; //cette écriture est équivalente a c= c+c;
	}
	

	/**
	 * Expliquez pourquoi la méthode ajoute n'a pas le même effet que la méthode enleve.
	 * Corrigez ajoute pour avoir le bon résultat.
	 */
	public static void main(String[] args) {
		c = 10;
		System.out.println("au départ "+ c);
		enleve(9);
		System.out.println("apres enleve = "+ c);
		ajoute(8);
		System.out.println("apres ajoute = "+ c);
	
	}

	
}
