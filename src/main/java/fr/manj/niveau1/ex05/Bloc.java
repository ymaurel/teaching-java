package fr.manj.niveau1.ex05;

public class Bloc {

	public static void main(String[] args) {
		int i = 0;

		{
			int c;
			c = 2;
			System.out.println(c);
		}

		// Décommentez la ligne System.out ..

		// A) La ligne ne compile pas, Pourquoi ?
		// => Deplacez la ligne "int c" pour régler le pb.

		// B) Faites une correction (sur la ligne int c et sur la ligne int i) pour que
		// le programme affiche 2 puis 10

		// System.out.println(i+c);
	}
}
