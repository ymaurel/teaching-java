package fr.manj.niveau2.ex04;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.Test;

import fr.manj.TestUtil;

public class ConversionsEntiersTest extends TestUtil {

	@Test
	public void testAfficheLaConversionOuErreurSiTropGrandErreur() {
		ConversionsEntiers.afficheLaConversionOuErreurSiTropGrand(2520000);
		assertEquals("ERREUR" + NL, this.outContent.toString());
	}

	@Test
	public void testAfficheLaConversionOuErreurSiTropGrandOk() {
		ConversionsEntiers.afficheLaConversionOuErreurSiTropGrand(25);
		assertEquals("25" + NL, this.outContent.toString());
	}

	@Test
	public void testMultiply() throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		Method meth = ConversionsEntiers.class.getMethod("multiply", byte.class, byte.class);
		byte a = 110;
		int result = Integer.valueOf(meth.invoke(null, new Object[] { a, a }).toString());
		assertEquals(a * a, result);
	}

}
