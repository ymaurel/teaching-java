package fr.manj.niveau2.ex01;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.manj.TestUtil;

public class ParamDeMainTest extends TestUtil {

	@Test
	public void test() {
		ParamDeMain.main(new String[] { "David", "Vincent" });
		assertEquals("Hello David Vincent" + NL, this.outContent.toString());
	}

}
