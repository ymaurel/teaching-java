package fr.manj.niveau2.ex05;

import static org.junit.Assert.*;

import org.junit.Test;

public class ConversionsFloatsTest {

	@Test
	public void testDivisionEntiere() {
		assertEquals(2,ConversionsFloats.divisionEntiere(4, 2));
		assertEquals(2,ConversionsFloats.divisionEntiere(5, 2));
		assertEquals(3,ConversionsFloats.divisionEntiere(6, 2));
	}
	
	@Test
	public void testDivision() {
		assertEquals(2,ConversionsFloats.division(4, 2), 0.1);
		assertEquals(2.5f,ConversionsFloats.division(5, 2), 0.1);
		assertEquals(3f, ConversionsFloats.division(6, 2), 0.1);
	}
	
	@Test
	public void testCelciusToFahrenheit() {
		assertEquals( 50,ConversionsFloats.celciusToFahrenheit(10), 0.1);
	}
}
