package fr.manj.niveau2.ex03;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.manj.TestUtil;

public class ConversionChainesTest extends TestUtil{

	@Test
	public void testStringToFloat() {
		assertEquals(ConversionChaines.stringToFloat("9.212"), 9.212f, 0.0001);
	}
	
	@Test
	public void testAfficheAvecTroisChiffresApresLaVirgure() {
		double c = 9.2132421;
		ConversionChaines.afficheAvecTroisChiffresApresLaVirgure(c);
	    assertEquals(String.format("%.3f", c), outContent.toString());
	}
	
	@Test
	public void testRetourneLaChaineAvecUnChiffresApresLaVirgure() {
		double c = 9.2132421;
		String result = ConversionChaines.retourneLaChaineAvecUnChiffresApresLaVirgure(c);
	    assertEquals(String.format("%.1f", c), result);
	}
}
