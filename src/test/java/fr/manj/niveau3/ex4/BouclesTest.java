package fr.manj.niveau3.ex4;

import static org.junit.Assert.*;

import org.junit.Test;

public class BouclesTest {

	@Test
	public void testFactFor() {
		assertEquals(6, Boucles.factAvecFor(3));
		assertEquals(3628800, Boucles.factAvecFor(10));
	}
	
	@Test
	public void testFactWhile() {
		assertEquals(6, Boucles.factAvecWhile(3));
		assertEquals(3628800, Boucles.factAvecWhile(10));
	}

	@Test
	public void testFactDoWhile() {
		assertEquals(6, Boucles.factAvecDoWhile(3));
		assertEquals(3628800, Boucles.factAvecDoWhile(10));
	}
	
}
