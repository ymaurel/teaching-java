package fr.manj.niveau3.ex5;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;


//Avec Java8, vous pouvez regarder : https://www.leveluplunch.com/java/examples/sum-values-in-array/
public class TableauxTest {


	int[] notes;

	@Before
	public void set() {
		notes = new int[] {10,15,20,10,15,20,15,20, 2 , 4, 12, 16,1};
	}
	
	@Test
	public void testCount() {
		
		assertEquals(Arrays.stream(notes).count(), Tableaux.count(notes));
	}

	@Test
	public void testSum() {
		assertEquals(Arrays.stream(notes).sum(), Tableaux.sum(notes));
	}
	
	@Test
	public void testAvg() {
		assertEquals(Arrays.stream(notes).max().getAsInt(), Tableaux.max(notes));
	}
	
	@Test
	public void testMin() {
		assertEquals(Arrays.stream(notes).min().getAsInt(), Tableaux.min(notes));
	}
	
	@Test
	public void testSort() {
		int[] copie = Tableaux.sort(notes);
		assertNotEquals(copie,notes);
		assertEquals(1,Arrays.stream(copie).min().getAsInt());
		assertEquals(20,Arrays.stream(copie).max().getAsInt());
		Arrays.sort(notes);
		assertArrayEquals(copie, notes);
	}
	
	@Test
	public void isSorted() {
		assertFalse("le tableaux n'est pas trié, mais la méthode dit que oui",Tableaux.isSorted(notes));
		Arrays.sort(notes);
		assertTrue("le tableaux est trié, mais la méthode dit que non", Tableaux.isSorted(notes));
	}
	
}
