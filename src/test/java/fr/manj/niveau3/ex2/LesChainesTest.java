package fr.manj.niveau3.ex2;

import static org.junit.Assert.*;

import org.junit.Test;

public class LesChainesTest {

	@Test
	public void testEgales() {
		assertTrue(LesChaines.egales("Test", new String("Test")));
		assertFalse(LesChaines.egales("Test", new String("Toto")));
	}
	@Test
	public void testIdentique() {
		assertFalse(LesChaines.identique("Test", new String("Test")));
		String s1 = "Test";
		String s2 = s1;
		assertTrue(LesChaines.identique(s1, s2));
	}
	
	@Test
	public void testConcat() {
		assertEquals("john SNOW",LesChaines.transforme("JoHn", "SNow"));
	}
}
