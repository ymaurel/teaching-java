package fr.manj.niveau3.ex1;

import static org.junit.Assert.*;

import org.junit.Test;

public class LesEnumsTest {

	@Test
	public void testToutesCouleurs() {
		assertEquals(6, LesEnums.Couleur.values().length);
	}

	@Test
	public void testVraieCouleur() {
		assertFalse("Blanc n'est pas une couleur", LesEnums.estVraieCouleur(LesEnums.Couleur.BLANC));
		assertTrue("Rouge est une couleur", LesEnums.estVraieCouleur(LesEnums.Couleur.ROUGE));
	}

	@Test
	public void testNoirEtBlanc() {
		assertTrue("Blanc est N&B", LesEnums.estNoirOuBlanc(LesEnums.Couleur.BLANC));
		assertFalse("Rouge est une couleur", LesEnums.estNoirOuBlanc(LesEnums.Couleur.ROUGE));
	}
	
}
