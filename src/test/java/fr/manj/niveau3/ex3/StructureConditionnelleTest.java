package fr.manj.niveau3.ex3;

import static org.junit.Assert.*;

import org.junit.Test;

public class StructureConditionnelleTest {

	@Test
	public void testAnimalToString() {
		assertEquals("chien", StructureConditionnelle.animalToString(StructureConditionnelle.Animal.CHIEN).toLowerCase());
		assertEquals("chat", StructureConditionnelle.animalToString(StructureConditionnelle.Animal.CHAT).toLowerCase());
		assertEquals("sfsdf", StructureConditionnelle.animalToString(StructureConditionnelle.Animal.AUTRE).toLowerCase());
	}
	
	@Test
	public void testStringToAnimal() {
		assertEquals(StructureConditionnelle.Animal.CHIEN, StructureConditionnelle.stringToAnimal("cHieN"));
		assertEquals(StructureConditionnelle.Animal.CHAT, StructureConditionnelle.stringToAnimal("ChAT"));
		assertEquals(StructureConditionnelle.Animal.AUTRE, StructureConditionnelle.stringToAnimal("cHieNLoup"));

	}

}
