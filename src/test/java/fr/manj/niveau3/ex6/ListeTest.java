package fr.manj.niveau3.ex6;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Ordering;


//Avec Java8, vous pouvez regarder : https://www.leveluplunch.com/java/examples/sum-values-in-array/
public class ListeTest {


	List<Integer> notes;

	@Before
	public void set() {
		notes = Arrays.asList(10,15,20,10,15,20,15,20, 2 , 4, 12, 16,1);
	}
	
	@Test
	public void testCount() {	
		assertEquals(notes.stream().count(), AvecDesListes.count(notes));
	}

	@Test
	public void testSum() {
		assertEquals(notes.stream().mapToInt(Integer::intValue).sum(), AvecDesListes.sum(notes));
	}
	
	@Test
	public void testAvg() {
		assertEquals(notes.stream().mapToInt(Integer::intValue).max().getAsInt(), AvecDesListes.max(notes));
	}
	
	@Test
	public void testMin() {
		assertEquals(notes.stream().mapToInt(Integer::intValue).min().getAsInt(), AvecDesListes.min(notes));
	}
	
	@Test
	public void testSort() {
		List<Integer> copie = AvecDesListes.sort(notes);
		assertTrue("la liste n'est pas triée", Ordering.natural().isOrdered(copie));
	}
	
	@Test
	public void isSorted() {
		assertFalse("le tableaux n'est pas trié, mais la méthode dit que oui",AvecDesListes.isSorted(Arrays.asList(4,3,20,1)));
		assertTrue("le tableaux est pas trié, mais la méthode dit que non",AvecDesListes.isSorted(Arrays.asList(1,2,3,4)));

	}
	
}
