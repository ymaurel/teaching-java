package fr.manj.niveau1.ex04;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.manj.TestUtil;
import fr.manj.niveau1.ex04.LireLesParametres;

public class LireLesParametresTest extends TestUtil{

	@Test
	public void testAffiche4() {
		LireLesParametres.main(null);
		assertEquals("4", outContent.toString());
	}

}
