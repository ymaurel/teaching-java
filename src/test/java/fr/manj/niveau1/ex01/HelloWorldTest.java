package fr.manj.niveau1.ex01;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import fr.manj.TestUtil;

/***
 * METHODES DE TESTS : Les méthodes ci-après permettent de vérifier que vous
 * avez bien réussi. Pour executer les tests faire : click droit > run as >
 * JUnit Test
 */

public class HelloWorldTest extends TestUtil {

	@Test
	public void testName() throws ClassNotFoundException {
		try {
			Class.forName("fr.manj.niveau1.ex01.HelloWorld");
		} catch (ClassNotFoundException ex) {
			fail("Le nom de la classe n'est pas le bon !");
		}
	}

	@Test
	public void testResult() throws ClassNotFoundException, NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		try {
			Class<?> cls = Class.forName("fr.manj.niveau1.ex01.HelloWorld");
			Method meth = cls.getMethod("main", String[].class);
			String[] params = null; // init params accordingly
			meth.invoke(null, (Object) params); // static method doesn't have an instance

			assertEquals("Hello World" + NL, this.outContent.toString());
		} catch (ClassNotFoundException ex) {
			fail("Le nom de la classe n'est pas le bon !");
		}
	}
}
