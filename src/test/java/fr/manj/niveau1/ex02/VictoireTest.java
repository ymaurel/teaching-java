package fr.manj.niveau1.ex02;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.Test;

import fr.manj.TestUtil;

public class VictoireTest extends TestUtil {
	@Test
	public void testMethodeMain() throws ClassNotFoundException, NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		try {
			Class<?> cls = Victoire.class;
			Method meth = cls.getMethod("main", String[].class);
		} catch (NoSuchMethodException ex) {
			fail("Pas la bonne méthode !");
		}
	}

	@Test
	public void testResultat() throws ClassNotFoundException, NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		try {
			Class<?> cls = Victoire.class;
			Method meth = cls.getMethod("main", String[].class);
			String[] params = null; // init params accordingly
			meth.invoke(null, (Object) params); // static method doesn't have an instance
			assertEquals("Victoire !", this.outContent.toString());
		} catch (NoSuchMethodException ex) {
			fail("Pas la bonne méthode !");
		}
	}
}
