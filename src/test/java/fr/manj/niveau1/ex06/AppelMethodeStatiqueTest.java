package fr.manj.niveau1.ex06;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import fr.manj.TestUtil;
import fr.manj.niveau1.ex05.Bloc;
import fr.manj.niveau1.ex06.AppelMethodeStatique;

public class AppelMethodeStatiqueTest extends TestUtil {

	@Test
	public void testAjoutFonctionne() {
		if (AppelMethodeStatique.add(3, 10) != 13) {
			fail("La fonction addition ne fonctionne pas comme souhaitée");
		}
	}

	@Test
	public void testAppelCorrect() {
		AppelMethodeStatique.main(null);
		assertEquals(AppelMethodeStatique.add(3, 5) + NL, this.outContent.toString());
	}
}
