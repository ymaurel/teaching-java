package fr.manj.niveau1.ex05;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.manj.TestUtil;
import fr.manj.niveau1.ex05.Bloc;

public class BlocTest extends TestUtil {

	@Test
	public void testAffiche2Puis10() {
		Bloc.main(null);
		assertEquals("2" + NL + "10" + NL, this.outContent.toString());
	}

}
