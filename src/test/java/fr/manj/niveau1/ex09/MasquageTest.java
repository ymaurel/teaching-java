package fr.manj.niveau1.ex09;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.manj.niveau1.ex09.Masquage;

public class MasquageTest {

	@Test
	public void testAjoute() {
		Masquage.c = 10;
		Masquage.ajoute(15);
		assertEquals(Masquage.c, 25);
	}

}
