package fr.manj.niveau1.ex08;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.manj.niveau1.ex08.LocalOuDeClasse;

public class LocalOuDeClasseTest {

	@Test
	public void testretourne10PlusCompteur1() {
		LocalOuDeClasse.compteur1 = 19;
		assertEquals(LocalOuDeClasse.retourneCompteur1Plus10(),29);
	}
	
	@Test
	public void testAjoute10ACompteur1() {
		LocalOuDeClasse.compteur1 = 10;
		LocalOuDeClasse.ajoute10ACompteur1();
		assertEquals(LocalOuDeClasse.compteur1,20);
	}

}
