package fr.manj.niveau1.ex03;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.manj.TestUtil;
import fr.manj.niveau1.ex03.ErrEtOut;

public class ErrEtOutTest extends TestUtil{

	@Test
	public void testQueOutAfficheHello() {
		ErrEtOut.main(null);
	    assertEquals("Hello", outContent.toString());
	}
	
	@Test
	public void testQueErrAfficheWorld() {
		ErrEtOut.main(null);
		assertEquals("World", errContent.toString());
	}

}
