package fr.manj.niveau1.ex07;

import static org.junit.Assert.*;
import static org.junit.Assert.fail;

import org.junit.Test;

import fr.manj.TestUtil;
import fr.manj.niveau1.ex05.Bloc;
import fr.manj.niveau1.ex07.AppelMethodeMath;

public class AppelMethodeMathTest extends TestUtil{

	@Test
	public void testinvSqrtPlus1() {
		assertTrue("La méthode invSqrtPlus1 est fausse", Math.abs((Math.pow(10, -0.5)+1) - AppelMethodeMath.invSqrtPlus1(10))< 0.1);
	}
	
	@Test
	public void testcosAPlusB() {
		assertTrue("La méthode cosAPlusB est fausse",(9.16092 - AppelMethodeMath.cosAPlusB(10,10))< 0.1);
	}
	
}
