package fr.manj;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;

import org.junit.After;
import org.junit.Before;

import fr.manj.niveau4.ex01.Point;

public class TestUtil {

	public static final String NL = System.getProperty("line.separator");

	public final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	public final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	public final PrintStream originalOut = System.out;
	public final PrintStream originalErr = System.err;

	@Before
	public void setUpStreams() {
		System.setOut(new PrintStream(this.outContent));
		System.setErr(new PrintStream(this.errContent));
	}

	@After
	public void restoreStreams() {
		System.setOut(this.originalOut);
		System.setErr(this.originalErr);
	}

	public static Object getFieldValue(String name, Object instance)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Field f = instance.getClass().getDeclaredField(name);
		f.setAccessible(true);
		return f.get(instance);
	}

}
