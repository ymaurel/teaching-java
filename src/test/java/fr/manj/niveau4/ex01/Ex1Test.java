package fr.manj.niveau4.ex01;

import static org.junit.Assert.*;

import java.lang.reflect.Field;

import org.junit.Test;

import fr.manj.niveau4.ex01.Ex1;
import fr.manj.niveau4.ex01.Point;

public class Ex1Test {

	@Test
	public void testQ1() {
		try {
			Ex1.question1();
		} catch (NullPointerException e) {
			fail("Vous n'avez pas intialisé le point");
		}
	}

	@Test
	public void testQ2()
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		try {
			Point p = Ex1.question2();

			// On se donne l'accès au champs :
			Field fx = Point.class.getDeclaredField("x");
			fx.setAccessible(true);

			Field fy = Point.class.getDeclaredField("y");
			fy.setAccessible(true);

			assertEquals(1.0, (double) fx.get(p), 0.001);
			assertEquals(4.0, (double) fy.get(p), 0.001);

		} catch (NullPointerException e) {
			fail("Vous n'avez pas intialisé le point");
		}
	}

}
