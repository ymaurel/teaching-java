package fr.manj.niveau4.ex2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.ObjectInputStream.GetField;
import java.lang.reflect.Constructor;

import org.junit.Test;

import fr.manj.TestUtil;

public class PersonneTest {

	@Test
	public void testConstructeur() {
		try {
			Constructor<Personne> c = Personne.class.getConstructor(String.class);
			Personne p = c.newInstance("Paul");
			assertEquals("Paul", TestUtil.getFieldValue("nom", p));
		} catch (Exception e) {
			fail("Vous n'avez pas implémenté le constructeur ou pas mis la bonne visibilité.");
		}
	}

	@Test
	public void testToString() {
		try {
			Constructor<Personne> c = Personne.class.getConstructor(String.class);
			Personne p = c.newInstance("Paul");
			assertEquals("Paul", p.toString());
		} catch (Exception e) {
			fail("Vous n'avez pas implémenté le constructeur ou pas mis la bonne visibilité.");
		}
	}

}
